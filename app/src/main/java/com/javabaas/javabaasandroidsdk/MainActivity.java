package com.javabaas.javabaasandroidsdk;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.javabaas.javasdk.JBConfig;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FormatStrategy formatStrategy = PrettyFormatStrategy.newBuilder()
                .showThreadInfo(true)  // (Optional) Whether to show thread info or not. Default true
                .methodCount(0)         // (Optional) How many method line to show. Default 2
                .methodOffset(7)        // (Optional) Hides internal method calls up to offset. Default 5
                .tag("JavaBaasAndroid")   // (Optional) Global tag for every log. Default PRETTY_LOGGER
                .build();

        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy));
        JBConfig.init("http://139.198.5.252:9000/api","59f85aee12a0aa06cb8539a7","4a4b67e87db24adbabfc5c64c1343dd2");

        /*JBQuery.getQuery("Test").countInBackground(new AndroidJBCountCallBack() {

            @Override
            public void aDone(boolean success, int count, JBException e) {
                Logger.w(success +" "+count + "  "+e);
                if (e != null){
                    e.printStackTrace();
                }
            }
        });*/
        /*JBQuery.getQuery("Test").findInBackground(new AndroidJBFindCallback<JBObject>() {
            @Override
            public void aDone(boolean success, List<JBObject> objects, JBException e) {
                Logger.w(success +" "+objects + "  "+e);
                if (e != null){
                    e.printStackTrace();
                }
            }
        });*/
        findViewById(R.id.user_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,UserActivity.class));
            }
        });

        findViewById(R.id.object_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,ObjectActivity.class));
            }
        });

    }
}
