package com.javabaas.javabaasandroidsdk;

import android.app.Dialog;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.javabaas.javasdk.JBException;
import com.javabaas.javasdk.JBObject;
import com.javabaas.javasdk.JBQuery;
import com.javabaas.javasdk.JBUser;
import com.javabaas.javasdk.callback.JBBooleanCallback;
import com.javabaas.javasdk.callback.JBFindCallBack;
import com.javabaas.javasdk.callback.JBLoginCallback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static java.text.DateFormat.FULL;
import static java.text.DateFormat.LONG;

public class ObjectActivity extends AppCompatActivity {

    private ArrayList<JBObject> data;
    private AlbumAdapter adapter;

    private String CLASS_NAME = "Album";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object);
        RecyclerView contentRv = findViewById(R.id.content_rv);
        contentRv.setLayoutManager(new LinearLayoutManager(this));

        data = new ArrayList<>();
        adapter = new AlbumAdapter(R.layout.album_item_layout, data);

        contentRv.setAdapter(adapter);

        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                JBObject jbObject = data.get(position);
                moreAction(jbObject);
            }
        });

        loadData();
    }

    private void loadData() {
        JBQuery.getQuery(CLASS_NAME).findInBackground(new JBFindCallBack<JBObject>() {
            @Override
            public void done(boolean success, List<JBObject> objects, JBException e) {
                if (success){
                    Toast.makeText(ObjectActivity.this,"查询成功",Toast.LENGTH_SHORT).show();
                    data.clear();
                    data.addAll(objects);
                    adapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(ObjectActivity.this,"查询失败",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.insert_btn:
                insert();
                break;
            case R.id.delete_btn:
                break;
            case R.id.update_btn:
                break;
            case R.id.query_btn:
                loadData();
                break;
        }
    }

    class AlbumAdapter extends BaseQuickAdapter<JBObject,BaseViewHolder>{

        public AlbumAdapter(int layoutResId, @Nullable List<JBObject> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, JBObject item) {

            helper.setText(R.id.content_tv,"id: "+ item.getObjectId()+"   title: "+item.get("title"));

        }
    }
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);

    private void insert(){
        JBObject jbObject = new JBObject(CLASS_NAME);
        String format = simpleDateFormat.format(new Date());
        jbObject.put("title", format +" insert");
        jbObject.saveInBackground(new JBBooleanCallback() {
            @Override
            public void done(boolean b, JBException e) {
                if (b){
                    Toast.makeText(ObjectActivity.this,"保存成功",Toast.LENGTH_SHORT).show();
                    loadData();
                }else {
                    Toast.makeText(ObjectActivity.this,"保存失败",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void moreAction(final JBObject item){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.album_dialog);
        final EditText usernameEt = dialog.findViewById(R.id.title_et);
        TextView idTv = dialog.findViewById(R.id.id_tv);
        idTv.setText(item.getObjectId());
        dialog.findViewById(R.id.update_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final String title = usernameEt.getText().toString().trim();
                if (TextUtils.isEmpty(title)) {
                    Toast.makeText(ObjectActivity.this, "title不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                view.setClickable(false);

                item.put("title",title);
                item.saveInBackground(new JBBooleanCallback() {
                    @Override
                    public void done(boolean success, JBException e) {
                        view.setClickable(true);
                        if (success) {
                            Toast.makeText(ObjectActivity.this, "更新成功!", Toast.LENGTH_SHORT).show();
                            loadData();
                        } else {
                            Toast.makeText(ObjectActivity.this, "更新失败!", Toast.LENGTH_SHORT).show();
                            if (e != null) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.delete_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                view.setClickable(false);

                item.deleteInBackground(new JBBooleanCallback() {
                    @Override
                    public void done(boolean success, JBException e) {
                        view.setClickable(true);
                        if (success) {
                            Toast.makeText(ObjectActivity.this, "删除成功!", Toast.LENGTH_SHORT).show();
                            loadData();
                        } else {
                            Toast.makeText(ObjectActivity.this, "删除失败!", Toast.LENGTH_SHORT).show();
                            if (e != null) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
