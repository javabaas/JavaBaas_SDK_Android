package com.javabaas.javabaasandroidsdk;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.javabaas.javasdk.JBException;
import com.javabaas.javasdk.JBUser;
import com.javabaas.javasdk.callback.JBBooleanCallback;
import com.javabaas.javasdk.callback.JBLoginCallback;

/**
 * Created by xsk on 2017/10/25.
 */

public class UserActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView statusTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        statusTv = findViewById(R.id.status_tv);
        View signUpTv = findViewById(R.id.sign_up_tv);
        View loginTv = findViewById(R.id.login_tv);
        View logoutTv = findViewById(R.id.logout_tv);

        signUpTv.setOnClickListener(this);

        loginTv.setOnClickListener(this);

        logoutTv.setOnClickListener(this);

        updateStatus();
    }

    private void updateStatus() {
        JBUser currentUser = JBUser.getCurrentUser();
        if (currentUser == null) {
            statusTv.setText("未登录!");
        } else {
            statusTv.setText("已登录 username : " +currentUser.getUsername());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_up_tv:
                signUp();
                break;
            case R.id.login_tv:
                login();
                break;
            case R.id.logout_tv:
                logout();
                break;
        }
    }

    private void logout(){
        if (JBUser.getCurrentUser() != null){
            //JBUser.resetSessionToken();
            JBUser.updateCurrentUser(null);
            updateStatus();
        }else {
            Toast.makeText(UserActivity.this, "尚未登录!", Toast.LENGTH_SHORT).show();
        }
    }

    private void login() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.login_dialig_layout);
        final EditText usernameEt = dialog.findViewById(R.id.username_et);
        final EditText passwordEt = dialog.findViewById(R.id.password_et);
        dialog.findViewById(R.id.login_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final String username = usernameEt.getText().toString().trim();
                final String password = passwordEt.getText().toString().trim();
                if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                    Toast.makeText(UserActivity.this, "用户名或密码不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                view.setClickable(false);
                JBUser.loginInBackground(username, password, new JBLoginCallback() {
                    @Override
                    public void done(boolean success, JBUser user, JBException e) {
                        view.setClickable(true);
                        dialog.dismiss();
                        if (success) {
                            Toast.makeText(UserActivity.this, "登录成功!", Toast.LENGTH_SHORT).show();
                            JBUser.updateCurrentUser(user);
                            updateStatus();
                        } else {
                            Toast.makeText(UserActivity.this, "登录失败!", Toast.LENGTH_SHORT).show();
                            if (e != null) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        });
        dialog.show();
    }

    //注册
    private void signUp() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.sign_up_dialig_layout);
        final EditText usernameEt = dialog.findViewById(R.id.username_et);
        final EditText passwordEt = dialog.findViewById(R.id.password_et);

        dialog.findViewById(R.id.sign_up_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final String username = usernameEt.getText().toString().trim();
                final String password = passwordEt.getText().toString().trim();
                if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                    Toast.makeText(UserActivity.this, "用户名或密码不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                view.setClickable(false);
                JBUser jbUser = new JBUser();
                jbUser.setUsername(username);
                jbUser.setPassword(password);
                jbUser.signUpInBackground(new JBBooleanCallback() {
                    @Override
                    public void done(boolean success, JBException e) {
                        view.setClickable(true);
                        dialog.dismiss();
                        if (success) {
                            Toast.makeText(UserActivity.this, "注册成功!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(UserActivity.this, "注册失败!", Toast.LENGTH_SHORT).show();
                            if (e != null) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
